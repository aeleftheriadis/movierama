#Installation Instructions#

###Install Bower###
Make sure you have bower installed if not execute the following
npm install -g bower

I have created a bower.json file so just run bower install
bower install -S

###Install Karma###
Install karma-cli globally
npm install -g karma-cli

Then we run npm install

I have configured karma with mocha, chai

###Files and Folders###
Files are located at src folder and tests under test

###Tests###
Start the tests with npm test

###Run npm run build###
