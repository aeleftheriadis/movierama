var   gulp = require('gulp'),
      concatCss = require('gulp-concat-css'),
      cssnano = require('gulp-cssnano'),
      concatJS = require('gulp-concat'),
      uglify = require('gulp-uglify');

gulp.task('js', function(){
 return gulp.src(
    [
      './bower_components/jquery/dist/jquery.js',
      './bower_components/bootstrap/dist/js/bootstrap.js',
      './bower_components/bootstrap-material-design/dist/js/material.js',
      './bower_components/bootstrap-material-design/dist/js/ripples.js',
      './bower_components/underscore/underscore.js',
      './src/app.js',
    ]
  )
 .pipe(concatJS('build.js'))
 .pipe(uglify())
 .pipe(gulp.dest('./dist/js/'));
});


// identifies a dependent task must be complete before this one begins
gulp.task('css', function() {
  return gulp.src
    (
      [
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './bower_components/bootstrap-material-design/dist/css/bootstrap-material-design.css',
        './bower_components/bootstrap-material-design/dist/css/ripples.css',
        './style.css'
      ]
    )
    .pipe(cssnano())
    .pipe(concatCss("./css/style.css"))
    .pipe(gulp.dest('./dist/'));
  });


gulp.task('default',['css','js'], function(){});
