$(function() {
  //Just to initialize the material design
  $.material.init();
});

//Movie Class
var MovieCollection = function(){
  this.page_limit = 10;
  this.page = 1;
  this.apikey =  'qtqep7qydngcc7grk4r4hyd9';
  this.title = 'Movies of the week';
  this.array = [];
  this.lastScrollTop = 0;
  this.searchValue = '';
}


var moviedetail;

//Rotten Tomatoes Service Revealing Module
var RottenTomatoes = (function () {

    function getWeeklyMovies(movies, successCallback,errorCallback) {
      $.ajax({
        url: "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json",
        type: "get", //send it through get method
        dataType: 'jsonp',
        data:{
          'page_limit':movies.page_limit,
          'page': movies.page,
          'country':'us',
          'apikey':movies.apikey
        },
        success: successCallback,
        error: errorCallback
      });
    }

    function searchMovies(movies,successCallback,errorCallback){
      $.ajax({
        url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
        type: "get", //send it through get method
        dataType: 'jsonp',
        data:{
          'page_limit':movies.page_limit,
          'page': movies.page,
          'q':movies.searchValue,
          'apikey':movies.apikey
        },
        success: successCallback,
        error: errorCallback
      });
    }

    function getMovie(id,successCallback,errorCallback){
      $.ajax({
        url: "http://api.rottentomatoes.com/api/public/v1.0/movies/"+id+".json",
        type: "get", //send it through get method
        dataType: 'jsonp',
        data:{
          'apikey':movies.apikey
        }
      }).done(function(data){
        moviedetail = {};
        moviedetail.id = data.id;
        moviedetail.genres = data.genres;
        moviedetail.abridged_directors = data.abridged_directors;
        $.ajax({
          url: "http://api.rottentomatoes.com/api/public/v1.0/movies/"+data.id+"/similar.json",
          type: "get", //send it through get method
          dataType: 'jsonp',
          data:{
            'apikey':movies.apikey,
            'limit':5
          }
        }).done(function(data){
          moviedetail.movies = data.movies;
          $.ajax({
            url: "http://api.rottentomatoes.com/api/public/v1.0/movies/"+moviedetail.id+"/reviews.json",
            type: "get", //send it through get method
            dataType: 'jsonp',
            data:{
              'apikey':movies.apikey,
              'review_type':'top_critic',
              'page_limit':2,
              'page':1,
              'country':'us'
            }
          }).done(function(data){
            moviedetail.reviews = data.reviews;
            $("#"+moviedetail.id+".movie-details").html(_.template($("#movie-details-template").html(),{moviedetail:moviedetail}));
          });
        });
      });
    }

    function getMoviesSuccess(data){
      movies.array = data.total <=10 ? data.movies : movies.array.concat(data.movies);
      if(movies.array.length <= data.total){
        $("#movieContainer").html(_.template($("#movie-template").html(),{movies:movies}));
      }
      else{
        movies.page = -1;
      }

    };

    function getMoviesError(error){
      throw new Error(error.statusText);
    }

    return {
        getWeeklyMovies: getWeeklyMovies,
        getMoviesSuccess: getMoviesSuccess,
        getMoviesError: getMoviesError,
        searchMovies:searchMovies,
        getMovie:getMovie
    };
}) ();

//Create a new Movie instance
var movies = new MovieCollection();

RottenTomatoes.getWeeklyMovies(movies,RottenTomatoes.getMoviesSuccess,RottenTomatoes.getMoviesError);

$(window).scroll(function(event){
  // check if we are near the bottom of the window
  if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    // where are we in the page?
    var scrollTop = $(this).scrollTop();
    // check if we are going down and we have not reached the total movies and there is no pending ajaxx request
    if (scrollTop > movies.lastScrollTop && movies.page !== -1 && $.active === 0){
      movies.page += 1;
      if(movies.searchValue.length>0){
        RottenTomatoes.searchMovies(movies,RottenTomatoes.getMoviesSuccess,RottenTomatoes.getMoviesError);
      }
      else{
        RottenTomatoes.getWeeklyMovies(movies,RottenTomatoes.getMoviesSuccess,RottenTomatoes.getMoviesError);
      }
    }
    movies.lastScrollTop = scrollTop;
  }
})

$(document).on('click','.js-show-details',function(){

  var getID = $(this).parents(".row").data("id");
  RottenTomatoes.getMovie(getID);
});

$(document).on('keyup','#search-input',function() {
  movies = new MovieCollection();
  movies.searchValue = $(this).val();

  if(movies.searchValue.length === 0){
    RottenTomatoes.getWeeklyMovies(movies,RottenTomatoes.getMoviesSuccess,RottenTomatoes.getMoviesError);
  }
  else{
    movies.title = 'Search for movie with parameter '+ movies.searchValue;
    //$("#movieContainer").html();
    RottenTomatoes.searchMovies(movies,RottenTomatoes.getMoviesSuccess,RottenTomatoes.getMoviesError);
  }
});
