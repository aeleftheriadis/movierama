var expect = chai.expect;

describe("A test suite for app", function() {
  describe("Test Movie Class",function(){
    var movieCollection = new MovieCollection();
    it('should return true if object is created successfully and all properties have the default values', function() {
      expect(movieCollection).to.exist; // passes
      expect(movieCollection).to.have.deep.property('page_limit')
        .that.is.an('number').that.deep.equals(10);
      expect(movieCollection).to.have.deep.property('page')
        .that.is.an('number').that.deep.equals(1);
      expect(movieCollection).to.have.deep.property('apikey')
        .that.is.an('string').that.deep.equals('qtqep7qydngcc7grk4r4hyd9');
      expect(movieCollection).to.have.deep.property('title')
        .that.is.an('string').that.deep.equals('Movies of the week');
      expect(movieCollection).to.have.property('array')
        .that.is.an('array').to.have.length(0);
      expect(movieCollection).to.have.deep.property('lastScrollTop')
        .that.is.an('number').that.deep.equals(0);
      expect(movieCollection).to.have.deep.property('searchValue')
        .that.is.an('string').to.have.length(0);
    });
    it('should set new movieCollection.searchValue to the new SearchParam',function(){
      movieCollection.searchValue = 'SearchParam';
      expect(movieCollection.searchValue).to.have.string('SearchParam')
    });
  });
});
